import 'dart:convert';

WGClient clientFromJson(String str) {
  final jsonData = json.decode(str);
  return WGClient.fromMap(jsonData);
}

String clientToJson(WGClient data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class WGClient {
  int id;
  String bird;
  double weight;
  double weekday;
  String note;
  String date;
//  DateTime date1;

  WGClient({
    this.id,
    this.bird,
    this.weight,
    this.weekday,
    this.note,
    this.date,
//    this.date1
  });

  factory WGClient.fromMap(Map<String, dynamic> json) => new WGClient(
        id: json["id"],
        bird: json["bird"],
        weight: json["weight"],
        weekday: json["weekday"],
        note: json["note"],
        date: json["date"],
//        date1: json["date1"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "bird": bird,
        "weight": weight,
        "weekday": weekday,
        "note": note,
        "date": date,
//        "date1": date1,
      };
}