import 'dart:convert';

FinClient clientFromJson(String str) {
  final jsonData = json.decode(str);
  return FinClient.fromMap(jsonData);
}

String clientToJson(FinClient data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class FinClient {
  int id;
  String title;
  String event;

  FinClient({
    this.id,
    this.title,
    this.event,
  });

  factory FinClient.fromMap(Map<String, dynamic> json) => new FinClient(
        id: json["id"],
        title: json["title"],
        event: json["event"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "event": event,
      };
}