import 'dart:convert';

Client clientFromJson(String str) {
  final jsonData = json.decode(str);
  return Client.fromMap(jsonData);
}

String clientToJson(Client data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Client {
  int id;
  String title;
  String event;
  String due;
  int isDone;

  Client({
    this.id,
    this.title,
    this.event,
    this.due,
    this.isDone
  });

  factory Client.fromMap(Map<String, dynamic> json) => new Client(
        id: json["id"],
        title: json["title"],
        event: json["event"],
        due: json["due"],
        isDone: json["isDone"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "event": event,
        "due": due,
        "isDone": isDone,
      };
}