import 'dart:convert';

GoalClient clientFromJson(String str) {
  final jsonData = json.decode(str);
  return GoalClient.fromMap(jsonData);
}

String clientToJson(GoalClient data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class GoalClient {
  int id;
  String goal;
  String step1;
  String step2;
  String step3;
  int finished1;
  int finished2;
  int finished3;



  GoalClient({
    this.id,
    this.goal,
    this.step1,
    this.step2,
    this.step3,
    this.finished1,
    this.finished2,
    this.finished3,

  });

  factory GoalClient.fromMap(Map<String, dynamic> json) => new GoalClient(
        id: json["id"],
        goal: json["goal"],
        step1: json["step1"],
        step2: json["step2"],
        step3: json["step3"],
        finished1: json["finished1"],
        finished2: json["finished2"],
        finished3: json["finished3"],



      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "goal": goal,
        "step1": step1,
        "step2": step2,
        "step3": step3,
        "finished1": finished1,
        "finished2": finished2,
        "finished3": finished3,
      };
}