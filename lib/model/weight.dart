import 'dart:convert';

WClient clientFromJson(String str) {
  final jsonData = json.decode(str);
  return WClient.fromMap(jsonData);
}

String clientToJson(WClient data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class WClient {
  int id;
  String bird;
  // String weight;
  // String note;
  // String date;

  WClient({
    this.id,
    this.bird,
    // this.weight,
    // this.note,
    // this.date,
  });

  factory WClient.fromMap(Map<String, dynamic> json) => new WClient(
        id: json["id"],
        bird: json["bird"],
        // weight: json["weight"],
        // note: json["note"],
        // date: json["date"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "bird": bird,
        // "weight": weight,
        // "note": note,
        // "date": date,
      };
}