import 'package:flutter/material.dart';

enum MyThemeKeys { LIGHT, DARK }

class MyThemes {
  
  static final ThemeData lightTheme = ThemeData(
    primaryColor: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
    backgroundColor: Color(0xFFeaeaeb),
    primaryTextTheme: TextTheme(
      
      bodyText2: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      bodyText1: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
      
    ),
    brightness: Brightness.light,
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
    backgroundColor: Color.fromARGB(255, 18, 32, 47),
    primaryTextTheme: TextTheme(
      bodyText1: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      bodyText2: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),

    ),
    brightness: Brightness.dark,
  );

  static ThemeData getThemeFromKey(MyThemeKeys themeKey) {
    switch (themeKey) {
      case MyThemeKeys.LIGHT:
        return lightTheme;
      case MyThemeKeys.DARK:
        return darkTheme;
      default:
        return darkTheme;
    }
  }
}
