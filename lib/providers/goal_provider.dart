import 'dart:io';

import '../model/goals.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class GoalProvider {
  GoalProvider._();
  static final GoalProvider db = GoalProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
    return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "GoalTest2Final.db");
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE GoalClient ("
          "id INTEGER PRIMARY KEY,"
          "goal TEXT,"
          "step1 TEXT,"
          "step2 TEXT,"
          "step3 TEXT,"
          "finished1 INTEGER,"
          "finished2 INTEGER,"
          "finished3 INTEGER"
          ")");
    });
  }

  
neWClient(GoalClient neWClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM GoalClient");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into GoalClient (id,goal,step1,step2,step3,finished1,finished2,finished3)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [id, neWClient.goal, neWClient.step1, neWClient.step2, neWClient.step3, neWClient.finished1, neWClient.finished2, neWClient.finished3]);
    return raw;
  }

  
updateGoal(GoalClient neWClient) async {
    final db = await database;
    var res = await db.update("GoalClient", neWClient.toMap(),
        where: "id = ?", whereArgs: [neWClient.id]);
    return res;
  }

getClient(int id) async {
    final db = await database;
    var res = await db.query("GoalClient", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? GoalClient.fromMap(res.first) : null;
  }

  Future<List<GoalClient>> getAllClients() async {
    final db = await database;
    var res = await db.query("GoalClient");
    List<GoalClient> list =
        res.isNotEmpty ? res.map((c) => GoalClient.fromMap(c)).toList() : [];
    return list;
  }

  deleteClient(int id) async {
    final db = await database;
    return db.delete("GoalClient", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from GoalClient");
  }

}