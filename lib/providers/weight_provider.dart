import 'dart:io';

import '../model/weight.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class WeightProvider {
  WeightProvider._();
  static final WeightProvider db = WeightProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
    return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Weight90.db");
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE WClient ("
          "id INTEGER PRIMARY KEY,"
          "bird TEXT"
          // "weight TEXT,"
          // "note TEXT,"
          // "date TEXT"
          ")");
    });
  }

  
neWClient(WClient neWClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM WClient");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into WClient (id,bird)"
        " VALUES (?,?)",
        [id, neWClient.bird]);
    return raw;
  }

  Future<bool> checkExist(String id) async {
  final db = await database;
  var queryResult = await db.rawQuery('SELECT * FROM WClient WHERE bird = ?', [id]);

  if(queryResult.isNotEmpty){
    return false;
  } else {
    return true;
  }
  }

  updateClient(WClient neWClient) async {
    final db = await database;
     var res = await db.update("WClient", neWClient.toMap(),
        where: "bird = ?", whereArgs: [neWClient.bird]);
    return res;
  }

getClient(int id) async {
    final db = await database;
    var res = await db.query("WClient", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? WClient.fromMap(res.first) : null;
  }

  Future<List<WClient>> getAllClients() async {
    final db = await database;
    var res = await db.query("WClient");
    List<WClient> list =
        res.isNotEmpty ? res.map((c) => WClient.fromMap(c)).toList() : [];
    return list;
  }

  deleteClient(int id) async {
    final db = await database;
    return db.delete("WClient", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from WClient");
  }

  deleteBird(String id) async {
    final db = await database;
    return db.delete("WClient", where: "bird = ?", whereArgs: [id]);
  }

}