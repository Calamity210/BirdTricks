import 'dart:io';
import 'dart:math';

import 'package:bezier_chart/bezier_chart.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_porter/utils/csv_utils.dart';

import '../model/graph.dart';

class WeightGraphProvider {
  WeightGraphProvider._();

  static final WeightGraphProvider db = WeightGraphProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "WeightGraph109.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE WGClient ("
              "id INTEGER PRIMARY KEY AUTOINCREMENT,"
              "bird TEXT,"
              "weight DOUBLE,"
              "weekday DOUBLE,"
              "note TEXT,"
              "date TEXT"
              ")");
        });
  }

  neWClient(WGClient neWClient) async {
    final db = await database;
    //get the biggest id in the table

    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM WGClient");
    int id = table.first["id"];

    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into WGClient (id,bird,weight, weekday, note, date)"
            " VALUES (?,?,?,?,?,?)",
        [
          id,
          neWClient.bird,
          neWClient.weight,
          neWClient.weekday,
          neWClient.note,
          neWClient.date
        ]);
    return raw;
  }

  Future<void> insertAllClients(List<List<dynamic>> data, String bird) async {
    for (List<dynamic> element in data) {
      if (element.first is String) continue;
      await neWClient(
          WGClient(
              bird: bird,
              weight: element[2],
              weekday: element[3],
              note: element[4],
              date: element[5]
          )
      );
    }
  }

  Future<void> toCSV(String bird) async {
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }
    var dbase = await db.database;
    var result = await dbase.query('WGClient');
    var csv = mapListToCsv(result);
    final directory = Platform.isAndroid ? "/sdcard/download/" : (await getApplicationDocumentsDirectory()).path;
    final pathToFile = directory +
        '/BirdTrickWeight$bird${Random().nextInt(10000)}.csv';
    File csvFile = File(pathToFile);
    csvFile.writeAsString(csv);

    print('Saved to: $pathToFile');
  }

  Future<bool> checkExist(String id) async {
    final db = await database;
    var queryResult =
    await db.rawQuery('SELECT * FROM WGClient WHERE bird = ?', [id]);

    if (queryResult.isNotEmpty) {
      return false;
    } else {
      return true;
    }
  }

  updateClient(WGClient neWClient) async {
    final db = await database;

    var res = await db.update("WGClient", neWClient.toMap(),
        where: "id = ?", whereArgs: [neWClient.id]);
    return res;
  }

  getClient(int id) async {
    final db = await database;
    var res = await db.query("WGClient", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? WGClient.fromMap(res.first) : null;
  }

  Future<List<WGClient>> getAllClients(String id) async {
    final db = await database;
    var res = await db.rawQuery('SELECT * FROM WGClient WHERE bird = "$id"');
    List<WGClient> list =
    res.isNotEmpty ? res.map((c) => WGClient.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<DataPoint<DateTime>>> returnValues(String id) async {
    final db = await database;
    var res = await db
        .rawQuery('SELECT * FROM WGClient WHERE bird = "$id" ORDER BY date');
    List<WGClient> list =
    res.isNotEmpty ? res.map((c) => WGClient.fromMap(c)).toList() : [];

    if (res.isNotEmpty) {
      return List.generate(list.length, (i) {
        final date = DateTime.parse(list[i].date);
        final date2 = DateTime.now();
        final int dif = date2
            .difference(date)
            .inDays;
        if (dif <= 372) {
          return DataPoint<DateTime>(
              xAxis: DateTime.parse(list[i].date), value: list[i].weight);
        } else {
          return DataPoint(value: 0, xAxis: DateTime.now());
        }
      });
    }
    return null;
  }

  Future deleteClient(int id) async {
    final db = await database;
    return db.delete("WGClient", where: "id = ?", whereArgs: [id]);
  }

  Future deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from WGClient");
  }

  Future deleteBird(String id) async {
    final db = await database;
    return db.delete("WGClient", where: "bird = ?", whereArgs: [id]);
  }
}
