import 'dart:io';
import '../model/finished_note.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class FinNoteProvider {
  FinNoteProvider._();
  static final FinNoteProvider db = FinNoteProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
    return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "FinishedNote.db");
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE FinClient ("
          "id INTEGER PRIMARY KEY,"
          "title TEXT,"
          "event TEXT"
          ")");
    });
  }

  
neWClient(FinClient neWClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM FinClient");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into FinClient (id,title,event)"
        " VALUES (?,?,?)",
        [id, neWClient.title, neWClient.event]);
    return raw;
  }

  updateClient(FinClient neWClient) async {
    final db = await database;
    var res = await db.update("FinClient", neWClient.toMap(),
        where: "id = ?", whereArgs: [neWClient.id]);
    return res;
  }

getClient(int id) async {
    final db = await database;
    var res = await db.query("FinClient", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? FinClient.fromMap(res.first) : null;
  }

  Future<List<FinClient>> getAllClients() async {
    final db = await database;
    var res = await db.query("FinClient");
    List<FinClient> list =
        res.isNotEmpty ? res.map((c) => FinClient.fromMap(c)).toList() : [];

    return list;
  }

  deleteClient(int id) async {
    final db = await database;
    return db.delete("FinClient", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from FinClient");
  }

}