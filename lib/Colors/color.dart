import 'dart:ui';
import 'package:flutter/material.dart';

final Map<int, Color> color = {
  50: Color.fromRGBO(255, 255, 255, .1),
  100: Color.fromRGBO(255, 255, 255, .2),
  200: Color.fromRGBO(255, 255, 255, .3),
  300: Color.fromRGBO(255, 255, 255, .4),
  400: Color.fromRGBO(255, 255, 255, .5),
  500: Color.fromRGBO(255, 255, 255, .6),
  600: Color.fromRGBO(255, 255, 255, .7),
  700: Color.fromRGBO(255, 255, 255, .8),
  800: Color.fromRGBO(255, 255, 255, .9),
  900: Color.fromRGBO(255, 255, 255, 1),
};

final MaterialColor white = MaterialColor(0xffffffff, color);

Color colorOne = white;
Color colorTwo = white[500];
Color colorThree = white[300];

final Map<int, Color> colorPink = {
  50: Color.fromRGBO(237, 45, 179, .1),
  100: Color.fromRGBO(237, 45, 179, .2),
  200: Color.fromRGBO(237, 45, 179, .3),
  300: Color.fromRGBO(237, 45, 179, .4),
  400: Color.fromRGBO(237, 45, 179, .5),
  500: Color.fromRGBO(237, 45, 179, .6),
  600: Color.fromRGBO(237, 45, 179, .7),
  700: Color.fromRGBO(237, 45, 179, .8),
  800: Color.fromRGBO(237, 45, 179, .9),
  900: Color.fromRGBO(237, 45, 179, 1),
};

final MaterialColor pink = MaterialColor(0xffed2db3, colorPink);

Color colorOnePink = pink;
Color colorTwoPink = pink[500];
Color colorThreePink = pink[300];
