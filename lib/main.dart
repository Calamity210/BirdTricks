import 'dart:async';

import 'package:BirdTricks/Themes/theme_widget.dart';
import 'package:BirdTricks/Themes/themes.dart';
import 'package:BirdTricks/Widgets/splash_screen.dart';
import 'package:BirdTricks/Widgets/waves.dart';
import 'package:BirdTricks/pages/shop.dart';
import 'package:BirdTricks/pages/task_goal.dart';
import 'package:BirdTricks/pages/weight.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mdi/mdi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:slimy_card/slimy_card.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  final bool isDark = prefs.getBool("isDark") ?? false;

  runApp(MaterialApp(
    home: CustomTheme(
      initialThemeKey: isDark ? MyThemeKeys.DARK : MyThemeKeys.LIGHT,
      child: MyApp(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: 'BirdTricks',
      debugShowCheckedModeBanner: false,
      theme: CustomTheme.of(context),
      home: SplashScreenWidget(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  int _page = 0;

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  void pageChanged(int index) {
    setState(() {
      _page = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: PageView(
        controller: pageController,
        onPageChanged: (index) {
          pageChanged(index);
        },
        children: <Widget>[
          InfoView(),
          WeightView(),
          TaskGoalView(),
          ShopView()
        ],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        index: _page,
        height: 55.0,
        color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
        buttonBackgroundColor: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
        backgroundColor: Theme.of(context).backgroundColor,
        animationCurve: Curves.easeOut,
        animationDuration: Duration(milliseconds: 500),
        items: <Widget>[
          Icon(Icons.info, size: 30, color: Colors.black),
          Icon(Mdi.scaleBathroom, size: 30, color: Colors.black),
          Icon(Icons.assignment, size: 30, color: Colors.black),
          Icon(FontAwesomeIcons.shoppingBag, size: 30, color: Colors.black),
        ],
        onTap: (index) {
          setState(() {
            pageController.jumpToPage(index);
          });
        },
      ),
    );
  }
}

class InfoView extends StatefulWidget {
  @override
  _InfoViewState createState() => _InfoViewState();
}

class _InfoViewState extends State<InfoView>
    with SingleTickerProviderStateMixin {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'BirdTricks',
          style: Theme.of(context)
              .primaryTextTheme
              .bodyText2
              .apply(color: Colors.black),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.solidMoon,
              // TODO MAKE DYNAMIC USING THEME COLOR
              color: Colors.black,
            ),
            onPressed: () {
              Theme.of(context).brightness == Brightness.light
                  ? _changeTheme(context, MyThemeKeys.DARK)
                  : _changeTheme(context, MyThemeKeys.LIGHT);
            },
          ),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.3,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/Birdtricks.jpg',
                    ),
                    fit: BoxFit.fill,
                  ),
                  color: Colors.white,
                ),
              ),
            ),
            Waves(150.0),
            SizedBox(
              height: 20,
            ),
            Align(
                alignment: Alignment.center,
                child: Text(
                  "Q & A",
                  style: GoogleFonts.abel(fontSize: 40),
                )),
            SizedBox(
              height: 60,
            ),
            SlimyCard(
              color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              width: MediaQuery.of(context).size.width * 0.9,
              topCardHeight: 200,
              bottomCardHeight: 400,
              borderRadius: 15,
              topCardWidget: Center(
                child: Text(
                  'Q: What Should I Be Feeding My Birds?',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              bottomCardWidget: Center(
                child: AutoSizeText(
                  "A: We, at BirdTricks, recommend a fresh, seasonal veggie chop in the morning (also called our bond-building breakfast) and a healthy, organic pellet in the evening. Foods such as fruits and nuts/seeds should only be fed as rewards for good behavior or training, and not in excessive amounts. To learn more about our Seasonal Feeding System that we recommend, along with our pellets that we recommend, please visit our website www.birdtricksstore.com and check out our Natural Feeding System Cookbooks along with our Life-Enhancing Organic Pellets for Parrots. ",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              slimeEnabled: true,
            ),
            SizedBox(
              height: 10,
            ),
            SlimyCard(
              color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              width: MediaQuery.of(context).size.width * 0.9,
              topCardHeight: 200,
              bottomCardHeight: 400,
              borderRadius: 15,
              topCardWidget: Center(
                child: Text(
                  'Q: Where should I start with my training?',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              bottomCardWidget: Center(
                child: AutoSizeText(
                  "A: That's a hard question to answer, as every person and bird, along with their past history and current level, are different. However, if you are wondering where to start, we always recommend starting with getting your bird onto a good diet, as this can alleviate almost 30% of behavioral problems on its own. Screaming, plucking, aggression - all of these can be helped by a change of diet (sometimes even fixed completely). Then, we usually recommend learning about the training quadrants and how the different quadrants can be used in training (along with what to avoid and what you might be doing accidentally). All of this, and so much more, is covered in our Beginner Level Course.",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              slimeEnabled: true,
            ),
            // Align(
            //   alignment: Alignment.topCenter,
            //   child: Container(
            //     width: MediaQuery.of(context).size.width * 0.9,
            //     height: MediaQuery.of(context).size.height * 0.8,
            //     decoration: BoxDecoration(
            //       shape: BoxShape.rectangle,
            //       borderRadius: BorderRadius.all(Radius.circular(25)),
            //       color: Colors.white,
            //     ),
            //     child: Card(
            //       shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.all(Radius.circular(25))),
            //       color: Colors.white,
            //       child: Stack(
            //         children: <Widget>[
            //           Align(
            //               alignment: Alignment.topCenter,
            //               child: Padding(
            //                 padding: const EdgeInsets.only(
            //                     top: 25.0, left: 10, right: 10),
            //                 child: Text(
            //                   'Q: Where should I start with my training?',
            //                   style:
            //                       TextStyle(color: Colors.pink, fontSize: 20),
            //                 ),
            //               )),
            //           Align(
            //             alignment: Alignment.topLeft,
            //             child: Padding(
            //               padding: const EdgeInsets.only(
            //                   left: 10, right: 10, top: 75),
            //               child: Text(
            //                 "A: That's a hard question to answer, as every person and bird, along with their past history and current level, are different. However, if you are wondering where to start, we always recommend starting with getting your bird onto a good diet, as this can alleviate almost 30% of behavioral problems on its own. Screaming, plucking, aggression - all of these can be helped by a change of diet (sometimes even fixed completely). Then, we usually recommend learning about the training quadrants and how the different quadrants can be used in training (along with what to avoid and what you might be doing accidentally). All of this, and so much more, is covered in our Beginner Level Course.",
            //                 style: TextStyle(color: Colors.black, fontSize: 16),
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(
              height: 10,
            ),

            SlimyCard(
              color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              width: MediaQuery.of(context).size.width * 0.9,
              topCardHeight: 200,
              bottomCardHeight: 400,
              borderRadius: 15,
              topCardWidget: Center(
                child: Text(
                  'Q: What happens if I get stuck on a goal and cannot seem to progress?',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              bottomCardWidget: Center(
                child: AutoSizeText(
                  "A: If you ever get stuck on a goal and it seems like you have hit a wall, we recommend first emailing us at info@birdtricks.com. Our customer service team, while not trainers, do have a lot of hands on experience and have all worked personally with Jamie and Dave. They may be able to give you a quick tip or two to get you moving in the right direction. However, if more than just a quick tip is required, we do offer 1-on-1 video consults that our team will be happy to recommend to you and give you more information about.",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              slimeEnabled: true,
            ),
            // Align(
            //   alignment: Alignment.topCenter,
            //   child: Container(
            //     width: MediaQuery.of(context).size.width * 0.9,
            //     height: MediaQuery.of(context).size.height * 0.8,
            //     decoration: BoxDecoration(
            //       shape: BoxShape.rectangle,
            //       borderRadius: BorderRadius.all(Radius.circular(25)),
            //       color: Colors.white,
            //     ),
            //     child: Card(
            //       shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.all(Radius.circular(25))),
            //       color: Colors.white,
            //       child: Stack(
            //         children: <Widget>[
            //           Align(
            //               alignment: Alignment.topCenter,
            //               child: Padding(
            //                 padding: const EdgeInsets.only(
            //                     top: 25.0, left: 10, right: 10),
            //                 child: Text(
            //                   'Q: What happens if I get stuck on a goal and cannot seem to progress?',
            //                   style:
            //                       TextStyle(color: Colors.pink, fontSize: 20),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               )),
            //           Align(
            //             alignment: Alignment.topLeft,
            //             child: Padding(
            //               padding: const EdgeInsets.only(
            //                   left: 10, right: 10, top: 100),
            //               child: Text(
            //                 "A: If you ever get stuck on a goal and it seems like you have hit a wall, we recommend first emailing us at info@birdtricks.com. Our customer service team, while not trainers, do have a lot of hands on experience and have all worked personally with Jamie and Dave. They may be able to give you a quick tip or two to get you moving in the right direction. However, if more than just a quick tip is required, we do offer 1-on-1 video consults that our team will be happy to recommend to you and give you more information about.",
            //                 style: TextStyle(color: Colors.black, fontSize: 16),
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(
              height: 10,
            ),

            SlimyCard(
              color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              width: MediaQuery.of(context).size.width * 0.9,
              topCardHeight: 200,
              bottomCardHeight: 400,
              borderRadius: 15,
              topCardWidget: Center(
                child: Text(
                  'Q: Why should I trick train my bird?',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              bottomCardWidget: Center(
                child: AutoSizeText(
                  "A: Trick training, while fun and entertaining, is also the best way to interact and bond with your bird in a positive way. It gets your bird thinking and gives them a challenge, something many birds miss out on in captivity. It also creates a language between you and your bird, and really allows you to dial in on their body language. Trick training, however, should not be attempted unless a correct diet is in place and the basic understandings of training techniques are in place. In summation, trick training gives your bird an energy outlet (helps with screaming and aggression), creates a common language between owner and bird (creating a closer bond and more trust), and challenges your bird (giving them self-confidence).",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              slimeEnabled: true,
            ),

            // Align(
            //   alignment: Alignment.topCenter,
            //   child: Container(
            //     width: MediaQuery.of(context).size.width * 0.9,
            //     height: MediaQuery.of(context).size.height * 0.8,
            //     decoration: BoxDecoration(
            //       shape: BoxShape.rectangle,
            //       borderRadius: BorderRadius.all(Radius.circular(25)),
            //       color: Colors.white,
            //     ),
            //     child: Card(
            //       shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.all(Radius.circular(25))),
            //       color: Colors.white,
            //       child: Stack(
            //         children: <Widget>[
            //           Align(
            //               alignment: Alignment.topCenter,
            //               child: Padding(
            //                 padding: const EdgeInsets.only(
            //                     top: 25.0, left: 10, right: 10),
            //                 child: Text(
            //                   'Q: Why should I trick train my bird?',
            //                   style:
            //                       TextStyle(color: Colors.pink, fontSize: 20),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               )),
            //           Align(
            //             alignment: Alignment.topLeft,
            //             child: Padding(
            //               padding: const EdgeInsets.only(
            //                   left: 10, right: 10, top: 75),
            //               child: Text(
            //                 "A: Trick training, while fun and entertaining, is also the best way to interact and bond with your bird in a positive way. It gets your bird thinking and gives them a challenge, something many birds miss out on in captivity. It also creates a language between you and your bird, and really allows you to dial in on their body language. Trick training, however, should not be attempted unless a correct diet is in place and the basic understandings of training techniques are in place. In summation, trick training gives your bird an energy outlet (helps with screaming and aggression), creates a common language between owner and bird (creating a closer bond and more trust), and challenges your bird (giving them self-confidence).",
            //                 style: TextStyle(color: Colors.black, fontSize: 16),
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(
              height: 10,
            ),

            SlimyCard(
              color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              width: MediaQuery.of(context).size.width * 0.9,
              topCardHeight: 200,
              bottomCardHeight: 400,
              borderRadius: 15,
              topCardWidget: Center(
                child: Text(
                  'Q: How long and often should training sessions be?',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              bottomCardWidget: Center(
                child: AutoSizeText(
                  "A: Formal training sessions should be kept to 3-5 minutes, as you don't want to burn out your bird or let them end the session on their terms. You can do these sessions though up to 3-4 times a day depending on if your bird is wanting to train or not. However, never train a bird when their motivation level is at a 5 - meaning, if they are desperate for training, that is NOT a good time to work with them. Instead, give them some food, and let them calm a bit on their own before initiating another training session. Information training sessions, such as capturing techniques or just low-key interactions, can be as frequent as desired, but if they are causing behavioral issues then they should be decreased.",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              slimeEnabled: true,
            ),
            // Align(
            //   alignment: Alignment.topCenter,
            //   child: Container(
            //     width: MediaQuery.of(context).size.width * 0.9,
            //     height: MediaQuery.of(context).size.height * 0.8,
            //     decoration: BoxDecoration(
            //       shape: BoxShape.rectangle,
            //       borderRadius: BorderRadius.all(Radius.circular(25)),
            //       color: Colors.white,
            //     ),
            //     child: Card(
            //       shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.all(Radius.circular(25))),
            //       color: Colors.white,
            //       child: Stack(
            //         children: <Widget>[
            //           Align(
            //               alignment: Alignment.topCenter,
            //               child: Padding(
            //                 padding: const EdgeInsets.only(
            //                     top: 25.0, left: 10, right: 10),
            //                 child: Text(
            //                   'Q: How long and often should training sessions be?',
            //                   style:
            //                       TextStyle(color: Colors.pink, fontSize: 20),
            //                   textAlign: TextAlign.center,
            //                 ),
            //               )),
            //           Align(
            //             alignment: Alignment.topLeft,
            //             child: Padding(
            //               padding: const EdgeInsets.only(
            //                   left: 10, right: 10, top: 75),
            //               child: Text(
            //                 "A: Formal training sessions should be kept to 3-5 minutes, as you don't want to burn out your bird or let them end the session on their terms. You can do these sessions though up to 3-4 times a day depending on if your bird is wanting to train or not. However, never train a bird when their motivation level is at a 5 - meaning, if they are desperate for training, that is NOT a good time to work with them. Instead, give them some food, and let them calm a bit on their own before initiating another training session. Information training sessions, such as capturing techniques or just low-key interactions, can be as frequent as desired, but if they are causing behavioral issues then they should be decreased.",
            //                 style: TextStyle(color: Colors.black, fontSize: 16),
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
