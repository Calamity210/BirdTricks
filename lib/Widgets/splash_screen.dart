import 'package:BirdTricks/main.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';

class SplashScreenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen.navigate(
      name: 'assets/splash.flr',
      next: (_) => MyHomePage(),
      until: () => Future.delayed(Duration(milliseconds: 100)),
      startAnimation: 'Untitled',
      backgroundColor: Color(0xffed2db3),

// TODO: CHANGE TO Color(0xffed2db3),
    );
  }
}
