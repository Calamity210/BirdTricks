import 'package:BirdTricks/model/finished_note.dart';
import 'package:BirdTricks/providers/finished_provider.dart';
import 'package:flutter/material.dart';

class DoneView extends StatefulWidget {
  DoneView({Key key}) : super(key: key);

  @override
  _DoneState createState() => _DoneState();
}

class _DoneState extends State<DoneView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Completed",
          style: Theme.of(context).primaryTextTheme.bodyText1.apply(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: FutureBuilder<List<FinClient>>(
          future: FinNoteProvider.db.getAllClients(),
          builder:
              (BuildContext context, AsyncSnapshot<List<FinClient>> snapshot) {
            int length;

            try {
              length = snapshot.data.length;
            } catch (NoSuchMethodError) {}

            if (length != null && length > 0) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemBuilder: (context, index) {
                    FinClient item = snapshot.data[index];

                    return Dismissible(
                      background: Container(
                        color: Colors.red,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                          child: Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      direction: DismissDirection.horizontal,
                      key: UniqueKey(),
                      onDismissed: (_) async {
                        FinNoteProvider.db.deleteClient(item.id);
                        String name = item.title;

                        Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text("$name Task Deleted")));
                      },
                      child: Card(
                        color: Color(0xff5d6169),
                        child: Row(
                          children: <Widget>[
                            Container(
                                color: Color(0xff17fc03),
                                width: 18,
                                height: 90),
                            Container(
                              color: Color(0xff5d6169),
                              width: MediaQuery.of(context).size.width - 30,
                              height: 90,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0,
                                    bottom: 30.0,
                                    left: 13.0,
                                    right: 22.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: _listTitle(item.title),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: snapshot.data.length,
                );
              } else {
                return Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 100.0),
                      child: Icon(
                        Icons.search,
                        size: 250.0,
                        color: Theme.of(context).primaryTextTheme.bodyText1.color,
                        semanticLabel: 'No Tasks Completed',
                      ),
                    ),
                    Center(
                      child: Text(
                        'No Completed Tasks',
                        style: TextStyle(
                          fontSize: 30,
                          color: Theme.of(context).primaryTextTheme.bodyText1.color,
                        ),
                      ),
                    ),
                  ],
                );
              }
            } else {
              return Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 100.0),
                    child: Icon(
                      Icons.search,
                      size: 250.0,
                      color: Theme.of(context).primaryTextTheme.bodyText1.color,
                      semanticLabel: 'No Tasks Completed',
                    ),
                  ),
                  Center(
                    child: Text(
                      'No Completed Tasks',
                      style: TextStyle(
                        fontSize: 30,
                        color: Theme.of(context).primaryTextTheme.bodyText1.color,
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  _listTitle(String title) {
    return Text(title,
        maxLines: 2,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white));
  }
}
