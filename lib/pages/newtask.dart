import 'package:BirdTricks/model/note.dart';
import 'package:BirdTricks/providers/note_provider.dart';
import 'package:beauty_textfield/beauty_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:rich_alert/rich_alert.dart';

class NewTaskView extends StatefulWidget {
  NewTaskView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _NewTaskState createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTaskView> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  String dateSelected;
  DateTime selDate = DateTime.now();
  String title;
  String _date = "Not Set";
  String _time = "Not Set";

  @override
  initState() {
    super.initState();
    _date = '${selDate.year} - ${selDate.month} - ${selDate.day}';
    _time = '${selDate.hour}:${selDate.minute}:${selDate.second}';

    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: null);
  }

  _scheduleNotification(DateTime dateScheduled, String task) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'BirdTricks', 'Birdtricks', 'Task Reminder',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(0, '$task',
        'Reminder for $task', dateScheduled, platformChannelSpecifics);
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop();
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text(
              "Add a New Task",
              style: Theme.of(context)
                  .primaryTextTheme
                  .bodyText1
                  .apply(color: Colors.black),
            ),
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context, false);
                }),
          ),
          body: SingleChildScrollView(
            child: Container(
              color: Theme.of(context).backgroundColor,
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: const Radius.circular(30.0),
                            bottomRight: const Radius.circular(30.0)),
                        image: new DecorationImage(
                          image: new AssetImage('assets/tasks_goals.jpg'),
                          fit: BoxFit.fill,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Card(
                      color: Color(0xFFffffff),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.55,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, top: 40.0, right: 30.0),
                              child: BeautyTextfield(
                                backgroundColor: Colors.white,
                                width: double.maxFinite,
                                height: 60,
                                duration: Duration(milliseconds: 300),
                                textColor: Colors.black,
                                accentColor: Colors.white,
                                inputType: TextInputType.text,
                                prefixIcon: Icon(
                                  Icons.text_fields,
                                  color: Colors.black,
                                ),
                                placeholder: "Title",
                                onTap: () {
                                  print('Click');
                                },
                                onChanged: (text) {
                                  setState(() {
                                    title = text;
                                  });
                                },
                                onSubmitted: (text) {
                                  setState(() {
                                    title = text;
                                  });
                                },
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    elevation: 4.0,
                                    onPressed: () {
                                      DatePicker.showDatePicker(
                                        context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        minTime: DateTime.now()
                                            .subtract(Duration(days: 360)),
                                        maxTime: DateTime.now()
                                            .add(Duration(days: 360)),
                                        onConfirm: (date) {
                                          print('confirm $date');
                                          var tempDate = DateTime(
                                            date.year,
                                            date.month,
                                            date.day,
                                            selDate.hour,
                                            selDate.minute,
                                            selDate.second,
                                            selDate.millisecond,
                                            selDate.microsecond,
                                          );
                                          setState(() {
                                            dateSelected = DateFormat(
                                                    'dd MMMM yyyy : hh:mm')
                                                .format(tempDate);
                                            selDate = tempDate;
                                            date = selDate;
                                            _date =
                                                '${date.year} - ${date.month} - ${date.day}';
                                          });
                                        },
                                        onChanged: (date) {
                                          print('confirm $date');
                                          var tempDate = DateTime(
                                            date.year,
                                            date.month,
                                            date.day,
                                            selDate.hour,
                                            selDate.minute,
                                            selDate.second,
                                            selDate.millisecond,
                                            selDate.microsecond,
                                          );
                                          setState(() {
                                            dateSelected = DateFormat(
                                                    'dd MMMM yyyy : hh:mm')
                                                .format(tempDate);
                                            selDate = tempDate;
                                            date = selDate;
                                            _date =
                                                '${date.year} - ${date.month} - ${date.day}';
                                          });
                                        },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en,
                                      );
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 50.0,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(
                                                      Icons.date_range,
                                                      size: 18.0,
                                                      color: Colors.teal,
                                                    ),
                                                    Text(
                                                      " $_date",
                                                      style: TextStyle(
                                                          color: Colors.teal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18.0),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          Text(
                                            "  Change",
                                            style: TextStyle(
                                                color: Colors.teal,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    elevation: 4.0,
                                    onPressed: () {
                                      DatePicker.showTime12hPicker(
                                        context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        onConfirm: (date) {
                                          var tempDate = DateTime(
                                            selDate.year,
                                            selDate.month,
                                            selDate.day,
                                            date.hour,
                                            date.minute,
                                            date.second,
                                            date.millisecond,
                                            date.microsecond,
                                          );

                                          setState(() {
                                            dateSelected = DateFormat(
                                                    'dd MMMM yyyy : hh:mm')
                                                .format(tempDate);
                                            selDate = tempDate;
                                            date = selDate;
                                            _time =
                                                '${date.hour}:${date.minute}:${date.second}';
                                          });
                                        },
                                        onChanged: (date) {
                                          var tempDate = DateTime(
                                            selDate.year,
                                            selDate.month,
                                            selDate.day,
                                            date.hour,
                                            date.minute,
                                            date.second,
                                            date.millisecond,
                                            date.microsecond,
                                          );

                                          setState(() {
                                            dateSelected = DateFormat(
                                                    'dd MMMM yyyy : hh:mm')
                                                .format(tempDate);
                                            selDate = tempDate;
                                            date = selDate;
                                            _time =
                                                '${date.hour}:${date.minute}:${date.second}';
                                          });
                                        },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en,
                                      );
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 50.0,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(
                                                      Icons.access_time,
                                                      size: 18.0,
                                                      color: Colors.teal,
                                                    ),
                                                    Text(
                                                      " $_time",
                                                      style: TextStyle(
                                                          color: Colors.teal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18.0),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          Text(
                                            "  Change",
                                            style: TextStyle(
                                                color: Colors.teal,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 45.0),
                                      child: ButtonTheme(
                                        minWidth: 200,
                                        height: 50,
                                        child: Builder(
                                          builder: (context) => RaisedButton(
                                              child: Text(
                                                'Add Task',
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .bodyText1,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          30)),
                                              color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                                              splashColor: Colors.yellow,
                                              elevation: 4.0,
                                              onPressed: () async {
                                                final title1 = title;
                                                final text = "Task/Reminder";
                                                final date = dateSelected;

                                                if (title1 == "" ||
                                                    text == "" ||
                                                    date == "" ||
                                                    dateSelected == null ||
                                                    selDate == null) {
                                                  print('here');
                                                } else {
                                                  dateSelected = "";
                                                  _scheduleNotification(
                                                      selDate, title);
                                                  await NoteProvider.db
                                                      .neWClient(
                                                    Client(
                                                        title: title1,
                                                        event: text,
                                                        due: date,
                                                        isDone: 0),
                                                  )
                                                      .then((_) {
                                                    Navigator.pop(context);
                                                    showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                            context) {
                                                          return RichAlertDialog(
                                                            alertTitle:
                                                                richTitle(
                                                                    "Success"),
                                                            alertSubtitle:
                                                                richSubtitle(
                                                                    "Task Successfully Added"),
                                                            alertType:
                                                                RichAlertType
                                                                    .SUCCESS,
                                                          );
                                                        });
                                                  });
                                                }
                                              }),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
