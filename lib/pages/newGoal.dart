import 'package:BirdTricks/model/goals.dart';
import 'package:BirdTricks/providers/goal_provider.dart';
import 'package:flutter/material.dart';
import 'package:rich_alert/rich_alert.dart';

Color pink = const Color(0xffed2db3);

class NewGoalView extends StatefulWidget {
  NewGoalView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _NewGoalState createState() => _NewGoalState();
}

class _NewGoalState extends State<NewGoalView> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _step1Controller = TextEditingController();
  final TextEditingController _step2Controller = TextEditingController();
  final TextEditingController _step3Controller = TextEditingController();

  @override
  void dispose() {
    _titleController.dispose();
    _step1Controller.dispose();
    _step2Controller.dispose();
    _step3Controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text(
              "Create a New Goal",
              style: Theme.of(context).primaryTextTheme.bodyText1.apply(color: Colors.black),
            ),
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context, false);
                }),
          ),
          body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Theme.of(context).backgroundColor,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0, top: 30.0),
                      child: Text(
                        'Goals',
                        style: TextStyle(
                          fontFamily: 'Poppins-Bold',
                          fontSize: 35,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryTextTheme.bodyText1.color,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Card(
                          color: Color(0xffffffff),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.67,
                            width: MediaQuery.of(context).size.width * 0.88,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 20.0),
                                  child: TextField(
                                    textInputAction: TextInputAction.done,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _titleController,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 1),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 0.0),
                                        ),
                                        labelText: 'Goal',
                                        hintText: 'Goal',
                                        hintStyle: TextStyle(fontSize: 18),
                                        labelStyle:
                                            TextStyle(color: Colors.black)),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    textInputAction: TextInputAction.done,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _step1Controller,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 1),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 0.0),
                                        ),
                                        labelText: 'Step 1',
                                        hintText: 'Step 1',
                                        hintStyle: TextStyle(fontSize: 18),
                                        labelStyle:
                                            TextStyle(color: Colors.black)),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    textInputAction: TextInputAction.done,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _step2Controller,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 1),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 0.0),
                                        ),
                                        labelText: 'Step 2',
                                        hintText: 'Step 2',
                                        hintStyle: TextStyle(fontSize: 18),
                                        labelStyle:
                                            TextStyle(color: Colors.black)),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    textInputAction: TextInputAction.done,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _step3Controller,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 1),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 0.0),
                                        ),
                                        labelText: 'Step 3',
                                        hintText: 'Step 3',
                                        hintStyle: TextStyle(fontSize: 18),
                                        labelStyle:
                                            TextStyle(color: Colors.black)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                  child: new Column(
                                    children: <Widget>[
                                      ButtonTheme(
                                        minWidth: 200,
                                        height: 50,
                                        child: Builder(
                                          builder: (context) => RaisedButton(
                                              child: Text(
                                                'Add Goal',
                                                style: Theme.of(context).primaryTextTheme.bodyText1,
                                              ),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          30)),
                                              color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                                              splashColor: Colors.yellow,
                                              elevation: 4.0,
                                              onPressed: () async {
                                                final goal =
                                                    _titleController.text;
                                                final step1 =
                                                    _step1Controller.text;
                                                final step2 =
                                                    _step2Controller.text;
                                                final step3 =
                                                    _step3Controller.text;

                                                if (goal == "" ||
                                                    step1 == "" ||
                                                    step2 == "" ||
                                                    step3 == "") {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return RichAlertDialog(
                                                          alertTitle: richTitle(
                                                              "Error"),
                                                          alertSubtitle:
                                                              richSubtitle(
                                                                  "All fields are required to be filled out"),
                                                          alertType:
                                                              RichAlertType
                                                                  .WARNING,
                                                        );
                                                      });
                                                } else {
                                                  setState(() {
                                                    _titleController.clear();
                                                    _step1Controller.clear();
                                                    _step2Controller.clear();
                                                    _step3Controller.clear();
                                                  });

                                                  await GoalProvider.db
                                                      .neWClient(GoalClient(
                                                    goal: goal,
                                                    step1: step1,
                                                    step2: step2,
                                                    step3: step3,
                                                    finished1: 0,
                                                    finished2: 0,
                                                    finished3: 0,
                                                  ))
                                                      .then((_) {
                                                    showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                            context) {
                                                          return RichAlertDialog(
                                                            alertTitle:
                                                                richTitle(
                                                                    "Success"),
                                                            alertSubtitle:
                                                                richSubtitle(
                                                                    "New Goal Created"),
                                                            alertType:
                                                                RichAlertType.SUCCESS
                                                          );
                                                        });
                                                  });
                                                  Navigator.pop(context);
                                                }
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
