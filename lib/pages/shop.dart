import 'package:flutter/foundation.dart';
// import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_native_web/flutter_native_web.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:undraw/undraw.dart';
import 'package:url_launcher/url_launcher.dart';

class ShopView extends StatefulWidget {
  ShopView({Key key}) : super(key: key);

  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<ShopView> {
  List<Map<String, String>> storeData = List<Map<String, String>>();

  @override
  void initState() {
    getData();
    super.initState();
  }

  Future<void> getData() async {
    var client = Client();
    final response =
        await client.get('https://birdtricksstore.com/collections/all');

    var document = parse(response.body);

    var prices = document
        .querySelectorAll('div.product-item__info > span.product-item__price');

    var links = document.querySelectorAll(
      'div.list-products > div.grid__cell > div.product-item > figure.product-item__image-container > a.product-item__link',
    );

    var imgs = document.querySelectorAll(
      'div.list-products > div.grid__cell > div.product-item > figure.product-item__image-container > a.product-item__link > div.product-item__image-wrapper > div.aspect-ratio > img.product-item__image',
    );

    prices.removeWhere(
        (element) => element.classes.contains('product-item__price--old'));

    for (int i = 0; i < imgs.length; i++) {
      var imgLink = imgs[i].attributes['data-src'].replaceAll('{width}', '800');

      print(imgs[i].attributes['alt'].replaceAll('- BirdTricksStore', ''));

      setState(() {
        storeData.add({
          'url': 'https://birdtricksstore.com' + links[i].attributes['href'],
          'imageUrl': imgLink,
          'title':
              imgs[i].attributes['alt'].replaceAll('- BirdTricksStore', ''),
          'price': prices[i].text
        });
      });
    }
  }

  // WebController webController;

  // void onWebCreated(webController) {
  //   this.webController = webController;
  //   this.webController.loadUrl("https://birdtricksstore.com/collections/all");
  //   this.webController.onPageStarted.listen((url) => print("Loading $url"));
  //   this
  //       .webController
  //       .onPageFinished
  //       .listen((url) => print("Finished loading $url"));
  // }

  Widget gridCell(Map<String, String> data) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () async {
          await launch(data['url']);
        },
        child: Card(
          elevation: 5,
            child: Column(
          children: <Widget>[
            Expanded(
              flex: 75,
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.85,
                  child: Image.network('https:' + data['imageUrl'])),
            ),
            Expanded(
              flex: 25,
              child: Column(
                children: <Widget>[
                  Text(
                    'BIRDTRICKS',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .bodyText1
                        .apply(fontSizeDelta: -5),
                  ),
                  Text(
                    data['title'],
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .primaryTextTheme
                        .bodyText1
                        .apply(fontSizeDelta: -1),
                  ),
                  Text(
                    data['price'],
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ],
        )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'BirdTricks Store',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        child: storeData.isNotEmpty
            ? GridView.count(
                primary: true,
                crossAxisCount: 1,
                childAspectRatio: 0.80,
                children: List.generate(storeData.length, (index) {
                  return gridCell(storeData[index]);
                }),
              )
            : Center(
                child: CircularProgressIndicator()
              ),
      ),
    );
  }
}
