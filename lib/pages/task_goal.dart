import 'package:BirdTricks/model/goals.dart';
import 'package:BirdTricks/model/note.dart';
import 'package:BirdTricks/pages/newGoal.dart';
import 'package:BirdTricks/pages/newtask.dart';
import 'package:BirdTricks/providers/goal_provider.dart';
import 'package:BirdTricks/providers/note_provider.dart';
import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:undraw/undraw.dart';

class TaskGoalView extends StatefulWidget {
  TaskGoalView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TaskGoalState createState() => _TaskGoalState();
}

class _TaskGoalState extends State<TaskGoalView>
    with SingleTickerProviderStateMixin {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Future<void> _cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffed2db3),
        child: Center(
          child: Icon(Icons.add),
        ),
        onPressed: () {
          if (_tabController.index == 0) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => NewTaskView()));
          } else {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => NewGoalView()));
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      appBar: AppBar(
        flexibleSpace: Image.asset('assets/words.jpg'),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(95.0),
          child: TabBar(
              controller: _tabController,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: <Widget>[
                Tab(text: "Tasks", icon: Icon(Icons.assignment, size: 25)),
                Tab(text: "Goals", icon: Icon(Icons.event_note, size: 25)),
              ]),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          Container(
            color: Theme.of(context).backgroundColor,
            child: FutureBuilder<List<Client>>(
              future: NoteProvider.db.getAllClients(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<Client>> snapshot) {
                int length;

                try {
                  length = snapshot.data.length;
                } catch (NoSuchMethodError) {}

                if (length != null && length > 0) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        Client item = snapshot.data[index];
                        return Dismissible(
                          background: Container(
                            color: Colors.red[400],
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          direction: DismissDirection.horizontal,
                          key: UniqueKey(),
                          onDismissed: (_) async {
                            await _cancelNotification();

                            NoteProvider.db.deleteClient(item.id);

                            String name = item.title;

                            Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text("$name Task Finished")));
                          },
                          child: Card(
                            color: Color(0xFFFFFFFF),
                            elevation: 5.0,
                            child: Container(
                              height: 90,
                              child: Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: item.isDone == 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.padded,
                                    onChanged: (bool x) {
                                      setState(() {
                                        NoteProvider.db.updateClient(
                                          Client(
                                            id: item.id,
                                            title: item.title,
                                            event: item.event,
                                            due: item.due,
                                            isDone: x == true ? 1 : 0,
                                          ),
                                        );
                                      });
                                    },
                                  ),
                                  Container(
                                    color: Color(0xFFFFFFFF),
                                    height: 90,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20.0),
                                          child: _listTitle(item.title),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10.0),
                                            child: _listText(
                                                'Due on: ' + item.due)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: snapshot.data.length,
                    );
                  } else {
                    return Center(
                      child: UnDraw(
                        color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                        illustration: UnDrawIllustration.no_data,
                        width: 200,
                        height: 200,
                      ),
                    );
                  }
                } else {
                  return Center(
                    child: UnDraw(
                      color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                      illustration: UnDrawIllustration.no_data,
                      width: 200,
                      height: 200,
                    ),
                  );
                  // return Center(
                  //   child: Container(
                  //     height: 60.0,
                  //     width: 230.0,
                  //     decoration: BoxDecoration(
                  //       borderRadius: new BorderRadius.circular(30),
                  //       color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
                  //     ),
                  //     child: Center(
                  //       child: const Text(
                  //         'Create A New Task',
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.bold, fontSize: 22),
                  //       ),
                  //     ),
                  //   ),
                  // );
                }
              },
            ),
          ),
          Container(
            color: Theme.of(context).backgroundColor,
            child: FutureBuilder<List<GoalClient>>(
              future: GoalProvider.db.getAllClients(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GoalClient>> snapshot) {
                int length;

                try {
                  length = snapshot.data.length;
                } catch (NoSuchMethodError) {}

                if (length != null && length > 0) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        GoalClient item = snapshot.data[index];
                        return Dismissible(
                            background: Container(
                              color: Colors.green[400],
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                child: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            direction: DismissDirection.horizontal,
                            key: UniqueKey(),
                            onDismissed: (_) async {
                              GoalProvider.db.deleteClient(item.id);
                              String name = item.goal;

                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text("$name Goal Completed")));
                            },
                            child: Card(
                              color: Colors.white,
                              elevation: 5.0,
                              child: Center(
                                child: ExpansionCard(
                                  trailing: Icon(Icons.keyboard_arrow_down, color: Colors.black),
                                  backgroundColor: Colors.white,
                                  margin: const EdgeInsets.only(
                                      top: 15, bottom: 15),
                                  title: Center(
                                    child: Text(
                                      item.goal,
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: item.finished1 == 1,
                                            materialTapTargetSize:
                                                MaterialTapTargetSize.padded,
                                            onChanged: (bool x) {
                                              setState(() {
                                                GoalProvider.db.updateGoal(
                                                    GoalClient(
                                                        id: item.id,
                                                        goal: item.goal,
                                                        step1: item.step1,
                                                        step2: item.step2,
                                                        step3: item.step3,
                                                        finished1:
                                                            x == true ? 1 : 0,
                                                        finished2:
                                                            item.finished2,
                                                        finished3:
                                                            item.finished3));
                                              });
                                            },
                                          ),
                                          Text(
                                            "Step 1: ${item.step1}",
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: item.finished2 == 1,
                                            materialTapTargetSize:
                                                MaterialTapTargetSize.padded,
                                            onChanged: (bool x) {
                                              setState(() {
                                                GoalProvider.db.updateGoal(
                                                    GoalClient(
                                                        id: item.id,
                                                        goal: item.goal,
                                                        step1: item.step1,
                                                        step2: item.step2,
                                                        step3: item.step3,
                                                        finished1:
                                                            item.finished1,
                                                        finished2:
                                                            x == true ? 1 : 0,
                                                        finished3:
                                                            item.finished3));
                                              });
                                            },
                                          ),
                                          Text(
                                            "Step 2: ${item.step2}",
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: item.finished3 == 1,
                                            materialTapTargetSize:
                                                MaterialTapTargetSize.padded,
                                            onChanged: (bool x) {
                                              setState(() {
                                                GoalProvider.db.updateGoal(
                                                  GoalClient(
                                                    id: item.id,
                                                    goal: item.goal,
                                                    step1: item.step1,
                                                    step2: item.step2,
                                                    step3: item.step3,
                                                    finished1: item.finished1,
                                                    finished2: item.finished2,
                                                    finished3:
                                                        x == true ? 1 : 0,
                                                  ),
                                                );
                                              });
                                            },
                                          ),
                                          Text(
                                            "Step 3: ${item.step3}",
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ));
                      },
                      itemCount: snapshot.data.length,
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                } else {
                  return Center(
                    child: UnDraw(
                      color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                      illustration: UnDrawIllustration.no_data,
                      width: 200,
                      height: 200,
                    ),
                  );
                  // return Center(
                  //   child: Container(
                  //     height: 60.0,
                  //     width: 230.0,
                  //     decoration: BoxDecoration(
                  //       borderRadius: new BorderRadius.circular(30),
                  //       color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
                  //     ),
                  //     child: Center(
                  //       child: const Text(
                  //         'Add A New Goal',
                  //         style: TextStyle(
                  //             fontWeight: FontWeight.bold, fontSize: 22),
                  //       ),
                  //     ),
                  //   ),
                  // );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

Widget _listText(String text) {
  return Text(
    text,
    style: TextStyle(
      color: Colors.black,
    ),
    maxLines: 3,
    overflow: TextOverflow.ellipsis,
  );
}

Widget _listTitle(String text) {
  return Text(text,
      maxLines: 2,
      style: TextStyle(
          fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black));
}
