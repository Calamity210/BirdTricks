import 'dart:convert';
import 'dart:io';

import 'package:BirdTricks/Widgets/graph_waves.dart';
import 'package:BirdTricks/model/graph.dart';
import 'package:BirdTricks/pages/newWeight.dart';
import 'package:BirdTricks/providers/graph_provider.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'package:csv/csv.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:intl/intl.dart';
import 'package:undraw/undraw.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

class GraphView extends StatefulWidget {
  GraphView(this.bird, {Key key}) : super(key: key);

  final String bird;

  @override
  _GraphState createState() => _GraphState(bird);
}

class _GraphState extends State<GraphView> {
  _GraphState(this.bird);

  final String bird;

  final TextEditingController _weightCont = TextEditingController();

  List colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.purple,
    Colors.deepOrange,
    Colors.lime,
    Colors.indigo
  ];

  Future<void> _exportDB() async {
    await WeightGraphProvider.db.toCSV(bird);
  }

  Future<void> _importCSV() async {
    var params = FlutterDocumentPickerParams(
      allowedFileExtensions: ['csv']
    );
    String filePath;
    try {
     filePath = await FlutterDocumentPicker.openDocument(params: params);
    } catch (e) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Please select a .csv file'),));
      return;
    }

    File csv = File(filePath);
    final input = csv.openRead();
    final fields = await input
        .transform(utf8.decoder)
        .transform(new CsvToListConverter())
        .toList();

    WeightGraphProvider.db
        .insertAllClients(fields, bird)
        .then((value) => setState(() {}));
  }

  void _handleSelection(String value) {
    switch (value) {
      case 'Export':
        _exportDB();

        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('CSV File Saved')));
        break;
      case 'Import':
        _importCSV();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xffed2db3),
          child: Center(
            child: Icon(Icons.add),
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => NewWeightView(bird)));
          },
        ),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Text(
            bird,
            style: Theme.of(context)
                .primaryTextTheme
                .bodyText1
                .apply(color: Colors.black),
          ),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context, false);
              }),
          actions: <Widget>[
            PopupMenuButton(
              tooltip: 'Menu',
              onSelected: _handleSelection,
              itemBuilder: (context) {
                return {'Export', 'Import'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            )
          ],
        ),
        body: Container(
          color: Theme.of(context).backgroundColor,
          child: Container(
            child: FutureBuilder<List<DataPoint<DateTime>>>(
                future: WeightGraphProvider.db.returnValues(bird),
                builder: (BuildContext context,
                    AsyncSnapshot<List<DataPoint<DateTime>>> snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: <Widget>[
                        Container(
                          height: 20,
                          color: Color(0xffed2db3),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width,
                          color: Color(0xff181818),
                          child: BezierChart(
                            fromDate:
                                DateTime.now().subtract(Duration(days: 365)),
                            bezierChartScale: BezierChartScale.WEEKLY,
                            toDate: DateTime.now(),
                            footerDateTimeBuilder: (dateTime, value) {
                              var format = DateFormat('EEE\nMMM d');
                              return format.format(dateTime);
                            },
                            selectedDate: DateTime.now(),
                            series: [
                              BezierLine(
                                  label: "Grams",
                                  onMissingValue: (dateTime) => 0.0,
                                  data: snapshot.data),
                            ],
                            config: BezierChartConfig(
                              verticalIndicatorStrokeWidth: 3.0,
                              verticalIndicatorColor: Colors.black26,
                              showVerticalIndicator: true,
                              verticalIndicatorFixedPosition: false,
                              footerHeight: 35.0,
                              backgroundColor: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                            ),
                          ),
                        ),
                        GraphWaves(200.0),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                        ),
                        FutureBuilder<List<WGClient>>(
                            future: WeightGraphProvider.db.getAllClients(bird),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<WGClient>> snapshot) {
                              if (snapshot.hasData) {
                                List<WGClient> list =
                                    snapshot.data.reversed.toList();
                                return SizedBox(
                                  height: 75.0,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {
                                      WGClient item = list[index];

                                      DateTime date = DateTime.parse(item.date);

                                      String month = DateFormat('MMM')
                                          .format(date)
                                          .toString();

                                      return Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10.0, right: 10),
                                        child: GestureDetector(
                                          onTap: () {
                                            showDialog(
                                                context: this.context,
                                                barrierDismissible: true,
                                                builder:
                                                    (BuildContext context) {
                                                  return AwesomeDialog(
                                                    context: context,
                                                    dialogType: DialogType.INFO,
                                                    animType:
                                                        AnimType.BOTTOMSLIDE,
                                                    btnOkText: "Edit",
                                                    btnCancelText: "Delete",
                                                    title:
                                                        '${item.bird}\'s Weight',
                                                    btnOkOnPress: () {
//                                                WeightGraphProvider.db.updateClient(neWClient);

                                                      showDialog(
                                                        context: context,
                                                        builder: (BuildContext
                                                                context) =>
                                                            CupertinoAlertDialog(
                                                          title: Text(
                                                              "Edit Weight"),
                                                          content: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 50.0,
                                                                    bottom:
                                                                        50.0),
                                                            child: Material(
                                                              color: Colors
                                                                  .transparent,
                                                              child: TextField(
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                textInputAction:
                                                                    TextInputAction
                                                                        .done,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 18,
                                                                  color: Colors
                                                                      .grey,
                                                                ),
                                                                controller:
                                                                    _weightCont,
                                                                decoration:
                                                                    InputDecoration(
                                                                  enabledBorder:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            25.0),
                                                                    borderSide: BorderSide(
                                                                        color: Colors
                                                                            .blue,
                                                                        width:
                                                                            1),
                                                                  ),
                                                                  border:
                                                                      OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            25.0),
                                                                    borderSide: BorderSide(
                                                                        color: Colors
                                                                            .blue,
                                                                        width:
                                                                            0.0),
                                                                  ),
                                                                  labelText:
                                                                      'New Weight',
                                                                  hintText:
                                                                      'New Weight',
                                                                  hintStyle: TextStyle(
                                                                      fontSize:
                                                                          18,
                                                                      color: Colors
                                                                          .grey),
                                                                  labelStyle:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .grey,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          actions: [
                                                            CupertinoDialogAction(
                                                              isDestructiveAction:
                                                                  true,
                                                              child: Text(
                                                                  'Cancel'),
                                                              onPressed: () =>
                                                                  Navigator.pop(
                                                                      context),
                                                            ),
                                                            CupertinoDialogAction(
                                                              isDefaultAction:
                                                                  true,
                                                              child: Text('Ok'),
                                                              onPressed:
                                                                  () async {
                                                                await WeightGraphProvider
                                                                    .db
                                                                    .updateClient(
                                                                  WGClient(
                                                                    id: item.id,
                                                                    bird: item
                                                                        .bird,
                                                                    weight: _weightCont
                                                                            .text
                                                                            .isNotEmpty
                                                                        ? double.parse(_weightCont
                                                                            .text)
                                                                        : item
                                                                            .weight,
                                                                    weekday: item
                                                                        .weekday,
                                                                    note: item
                                                                        .note,
                                                                    date: item
                                                                        .date,
                                                                  ),
                                                                );

                                                                Navigator.pop(
                                                                    context);
                                                                Navigator.pop(
                                                                    context);
                                                                setState(() {});
                                                              },
                                                            )
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                    btnCancelOnPress: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                      WeightGraphProvider.db
                                                          .deleteClient(
                                                              item.id);

                                                      setState(() {});
                                                    },
                                                    desc:
                                                        "Bird: ${item.bird}\nDate: ${DateFormat('dd MMM yyyy : hh:mm').format(date).toString()}\nWeight: ${item.weight}\nNote: ${item.note}",
                                                  ).show();
                                                });
                                          },
                                          child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                color: colors[date.weekday - 1],
                                              ),
                                              height: 75,
                                              width: 75,
                                              child: Stack(
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            bottom: 3.0),
                                                    child: Align(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Text(
                                                          date.day.toString(),
                                                          style: TextStyle(
                                                              fontSize: 24,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            bottom: 3.0),
                                                    child: Align(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Text(
                                                        month,
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                        ),
                                      );
                                    },
                                    itemCount: snapshot.data.length,
                                  ),
                                );
                              } else {
                                return Center(
                                  child: UnDraw(
                                    color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                                    illustration: UnDrawIllustration.no_data,
                                    width: 200,
                                    height: 200,
                                  ),
                                );
                              }
                            }),
                      ],
                    );
                  } else {
                    return Center(
                      child: UnDraw(
                        color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                        illustration: UnDrawIllustration.no_data,
                        width: 200,
                        height: 200,
                      ),
                    );
                  }
                }),
          ),
        ));
  }
}
