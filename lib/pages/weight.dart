import 'package:BirdTricks/model/weight.dart';
import 'package:BirdTricks/pages/graph.dart';
import 'package:BirdTricks/providers/graph_provider.dart';
import 'package:BirdTricks/providers/weight_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:undraw/undraw.dart';

class WeightView extends StatefulWidget {
  WeightView({Key key}) : super(key: key);

  @override
  _WeightState createState() => _WeightState();
}

class _WeightState extends State<WeightView> {
  SharedPreferences prefs;
  TextEditingController _birdCont = new TextEditingController();

  @override
  void dispose() {
    _birdCont.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Image.asset('assets/words.jpg'),
        bottom: AppBar(
          centerTitle: true,
          title: Text(
            "Weight",
            style: Theme.of(context)
                .primaryTextTheme
                .bodyText1
                .apply(color: Colors.black),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffed2db3),
        child: Center(
          child: Icon(Icons.add),
        ),
        onPressed: () async {
          showDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text("Add a New Bird"),
              content: Padding(
                padding: const EdgeInsets.only(top: 50.0, bottom: 50.0),
                child: Material(
                  color: Colors.transparent,
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                    ),
                    controller: _birdCont,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(color: Colors.blue, width: 1),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(color: Colors.blue, width: 0.0),
                      ),
                      labelText: 'Bird Name',
                      hintText: 'Bird Name',
                      hintStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      labelStyle: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              actions: [
                CupertinoDialogAction(
                  isDestructiveAction: true,
                  child: Text('Cancel'),
                  onPressed: () => Navigator.pop(context),
                ),
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text('Ok'),
                  onPressed: () async {
                    await WeightProvider.db.neWClient(
                      WClient(
                        bird: _birdCont.text,
                      ),
                    );

                    Navigator.pop(context);

                    setState(() {});
                  },
                )
              ],
            ),
          );

          // Navigator.push(context,
          //     MaterialPageRoute(builder: (context) => NewWeightView()));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: FutureBuilder<List<WClient>>(
          future: WeightProvider.db.getAllClients(),
          builder:
              (BuildContext context, AsyncSnapshot<List<WClient>> snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
            }

            int length;

            try {
              length = snapshot.data.length;
            } catch (NoSuchMethodError) {}

            if (length != null && length > 0) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemBuilder: (context, index) {
                    WClient item = snapshot.data[index];

                    return Dismissible(
                      background: Container(
                        color: Colors.red,
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                      ),
                      direction: DismissDirection.horizontal,
                      key: UniqueKey(),
                      onDismissed: (_) async {
                        WeightGraphProvider.db.deleteBird(item.bird);

                        WeightProvider.db.deleteClient(item.id);

                        Scaffold.of(context)
                            .showSnackBar(SnackBar(content: Text("Deleted")));
                      },
                      child: GestureDetector(
                        onTap: () async {
                          //var page = await buildPageAsync();
                          var route = MaterialPageRoute(
                              builder: (_) => GraphView(item.bird));
                          Navigator.push(context, route);
                        },
                        child: Card(
                          color: Color(0xff5d6169),
                          child: Row(
                            children: <Widget>[
                              Container(
                                color: Color(0xffeb9534),
                                width: MediaQuery.of(context).size.width * 0.05,
                                height: 100,
                              ),
                              Container(
                                color: Color(0xff5d6169),
                                width: MediaQuery.of(context).size.width * 0.88,
                                height: 100,
                                child: Center(
                                  child: _listTitle(item.bird),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: snapshot.data.length,
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            } else {
              return Center(
                child: UnDraw(
                  color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                  illustration: UnDrawIllustration.no_data,
                  width: 200,
                  height: 200,
                ),
              );
              // return Center(
              //   child: Container(
              //     height: 60.0,
              //     width: 265.0,
              //     decoration: BoxDecoration(
              //       borderRadius: new BorderRadius.circular(30),
              //       color: Colors.white,
// TODO: CHANGE TO Color(0xffed2db3),
              //     ),
              //     child: Center(
              //       child: const Text(
              //         'Track Weight',
              //         style:
              //             TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
              //       ),
              //     ),
              //   ),
              // );
            }
          },
        ),
      ),
    );
  }

  Widget _listTitle(String text) {
    return Text(text,
        maxLines: 2,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white));
  }
}
