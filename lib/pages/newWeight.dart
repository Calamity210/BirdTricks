import 'package:BirdTricks/model/graph.dart';
import 'package:BirdTricks/model/weight.dart';
import 'package:BirdTricks/pages/graph.dart';
import 'package:BirdTricks/providers/graph_provider.dart';
import 'package:BirdTricks/providers/weight_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:rich_alert/rich_alert.dart';

Color pink = const Color(0xffed2db3);

class NewWeightView extends StatefulWidget {
  NewWeightView(this.birdName, {Key key, this.title}) : super(key: key);

  final String title;
  final String birdName;

  @override
  _NewWeightState createState() => _NewWeightState(birdName);
}

class _NewWeightState extends State<NewWeightView> {
  _NewWeightState(this.birdName);

  final String birdName;
  String dateSelected;
  DateTime selDate = DateTime.now();
  String _date = "Not Set";

  final TextEditingController _weightController = TextEditingController();
  final TextEditingController _weightNoteController = TextEditingController();

  @override
  void initState() {
    _date =
    '${selDate.year} - ${selDate.month} - ${selDate.day}';
    super.initState();
  }

  @override
  void dispose() {
    _weightController.dispose();
    _weightNoteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text(
              "Add a New Weight",
              style: Theme.of(context).primaryTextTheme.bodyText1.apply(color: Colors.black),
            ),
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context, false);
                  setState(() {});
                }),
          ),
          body: SingleChildScrollView(
            child: Container(
              color: Theme.of(context).backgroundColor,
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: const Radius.circular(35.0),
                            bottomRight: const Radius.circular(35.0)),
                        image: new DecorationImage(
                          image: new AssetImage('assets/weight.jpg'),
                          fit: BoxFit.fill,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Card(
                      color: Color(0xffffffff),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.55,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0,
                                  right: 15.0,
                                  left: 15.0,
                                  bottom: 15.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10.0, bottom: 10.0),
                                    child: TextField(
                                      textInputAction: TextInputAction.done,
                                      textCapitalization:
                                          TextCapitalization.words,
                                      style: TextStyle(
                                          fontSize: 18, color: Colors.black),
                                      controller: _weightController,
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                            borderSide: BorderSide(
                                                color: Colors.black, width: 1),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                            borderSide: BorderSide(
                                                color: Colors.black,
                                                width: 0.0),
                                          ),
                                          labelText: 'Weight',
                                          hintText: 'Weight',
                                          hintStyle: TextStyle(fontSize: 18),
                                          labelStyle:
                                              TextStyle(color: Colors.black)),
                                      keyboardType: TextInputType.number,
                                    ),
                                  ),
                                  TextField(
                                    textInputAction: TextInputAction.done,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.black),
                                    controller: _weightNoteController,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 1.0),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 0.0),
                                        ),
                                        labelText: 'Notes',
                                        hintText: 'Notes(Optional)',
                                        hintStyle: TextStyle(fontSize: 18),
                                        labelStyle:
                                            TextStyle(color: Colors.black)),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top:10.0),
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0)),
                                      elevation: 4.0,
                                      onPressed: () {
                                        DatePicker.showDatePicker(
                                          context,
                                          theme: DatePickerTheme(
                                            containerHeight: 210.0,
                                          ),
                                          showTitleActions: true,
                                          minTime: DateTime.now()
                                              .subtract(Duration(days: 360)),
                                          maxTime: DateTime.now()
                                              .add(Duration(days: 360)),
                                          onConfirm: (date) {
                                            print('confirm $date');
                                            setState(() {
                                            selDate = date;
                                              dateSelected =
                                                DateFormat('dd MMMM yyyy : hh:mm')
                                                    .format(date);
                                            _date =
                                                '${date.year} - ${date.month} - ${date.day}';
                                            });
                                            // dateSelected =
                                            //     DateFormat('dd MMMM yyyy : hh:mm')
                                            //         .format(date);
                                            // selDate = date;
                                          },
                                          onChanged: (date) {
                                            setState(() {
                                            selDate = date;
                                              dateSelected =
                                                DateFormat('dd MMMM yyyy : hh:mm')
                                                    .format(date);
                                            });
                                          },
                                          currentTime: DateTime.now(),
                                          locale: LocaleType.en,
                                        );
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 50.0,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.date_range,
                                                        size: 18.0,
                                                        color: Colors.teal,
                                                      ),
                                                      Text(
                                                        " $_date",
                                                        style: TextStyle(
                                                            color: Colors.teal,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18.0),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                            Text(
                                              "  Change",
                                              style: TextStyle(
                                                  color: Colors.teal,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0),
                                            ),
                                          ],
                                        ),
                                      ),
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20.0),
                              child: new Column(
                                children: <Widget>[
                                  ButtonTheme(
                                    minWidth: 200,
                                    height: 50,
                                    child: Builder(
                                      builder: (context) => RaisedButton(
                                          child: Text(
                                            'Save Weight',
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .bodyText1,
                                          ),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      30)),
                                          color: Color(0xffed2db3),
// TODO: CHANGE TO Color(0xffed2db3),
                                          splashColor: Colors.yellow,
                                          elevation: 4.0,
                                          onPressed: () async {
                                            final bird = birdName;
                                            final weight =
                                                _weightController.text;
                                            final weight1 =
                                                double.parse(weight.toString());
                                            final note =
                                                _weightNoteController.text;

                                            final weekday =
                                                DateTime.now().weekday;
                                            final weekday1 = double.parse(
                                                weekday.toString());

                                            if (bird == "" || weight == "") {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return RichAlertDialog(
                                                      alertTitle:
                                                          richTitle("Error"),
                                                      alertSubtitle: richSubtitle(
                                                          "Some fields are required to be filled out"),
                                                      alertType:
                                                          RichAlertType.WARNING,
                                                    );
                                                  });
                                            } else {
                                              _weightController.text = "";
                                              _weightNoteController.text = "";

                                              await WeightGraphProvider.db
                                                  .neWClient(
                                                WGClient(
                                                  bird: bird,
                                                  weight: weight1,
                                                  weekday: weekday1,
                                                  note: note,
                                                  date: selDate.toString() ??
                                                      DateTime.now().toString(),
                                                ),
                                              );

                                              await WeightProvider.db
                                                  .deleteBird(bird)
                                                  .then((_) {
                                                WeightProvider.db.neWClient(
                                                  WClient(
                                                    bird: bird,
                                                    // weight: weight
                                                    //     .toString(),
                                                    // note: note,
                                                    // date: date
                                                  ),
                                                );

                                                Navigator.pop(context);
                                                Navigator.pop(context);
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            GraphView(
                                                                birdName)));

                                                setState(() {});
                                              });
                                            }
                                          }),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
